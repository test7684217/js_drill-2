exports.problem2 = (data) => {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array.");
    }

    if(data.length==0){
       throw new Error("Input data is empty");
    } 

    let last_car = data[data.length - 1];
    console.log(`Last car is a ${last_car.car_make} ${last_car.car_model}`);

  } catch (error) {
    console.log(error.message);
  }
};
