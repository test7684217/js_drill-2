exports.problem4 = (data) => {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array");
    }

    return data.map((item) => {
      if (item.car_year){
        return item.car_year;
      }
    });

  } catch (error) {
    console.log(error.message);
    return [];
  }
};
