exports.problem6 = (data) => {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array");
    }
    return data.filter((item) => {
      if ((item.car_make) && (item.car_make === "Audi" || item.car_make == "BMW")) {
        return item;
      }
    });
  } catch (error) {
    console.log(error.message);
    return []
  }
};
