exports.problem5 = (data) => {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array");
    }

  return data
    .filter((item) => {
      if(item.car_year < 2000){
        return item.car_year;
      }
    })
    
  }
  catch(error){
    console.log(error.message)
    return [];
  }
};
