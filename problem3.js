exports.problem3 = (data) => {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array");
    }
    let arr = [...data];
    let carModels = arr.map((item) => {
        if (item.car_model){ 
          return item.car_model;
        }
      })
      .sort((a, b) => { 
        return a.toLowerCase().localeCompare(b.toLowerCase())
      });

      return carModels;

  } catch (error) {
    console.log(error.message);
    return []
  }
};
