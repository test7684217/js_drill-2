function problem1(data) {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array.");
    }

    let carFound = false;
    data.filter((item) => {
      if (item.id == 33) {
        console.log(
          `Car 33 is a ${item.car_year} ${item.car_make} ${item.car_model}`
        );
        carFound = true;
        
      }
    });

    if (!carFound) {
      console.log("Car with ID 33 not found.");
    }
  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem1;
